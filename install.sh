#!/usr/bin/env bash
# This script sets basic parameters for working at claix23
# during a research project in the Klankermayer group
# (c) 2024 - Ole Osterthun

set -euo pipefail

RESET="\e[0m"
RED="\e[0;31m"
GREEN="\e[0;32m"
YELLOW="\e[0;33m"
BLUE="\e[0;34m"

TICK="[\e[0;32m\u2713\e[0m]"
CROSS="[\e[0;31m\u2717\e[0m]"

USER_GROUPS=(orca gaussian p0020510)

show_logo() {
	cat <<"logo"
	      _             _               
	     | |           | |              
	  ___| |_ __ _ _ __| |_ _   _ _ __  
	 / __| __/ _` | '__| __| | | | '_ \ 
	 \__ \ || (_| | |  | |_| |_| | |_) |
	 |___/\__\__,_|_|   \__|\__,_| .__/ 
	                             | |    
	       _       _      ___  __|_|    
	      | |     (_)    |__ \|___ \    
	   ___| | __ _ ___  __  ) | __) |   
	  / __| |/ _` | \ \/ / / / |__ <    
	 | (__| | (_| | |>  < / /_ ___) |   
	  \___|_|\__,_|_/_/\_\____|____/    
logo
}

check_user() {
	#local user=$(whoami)
	local groups
	groups=$(id)
	for group in "${USER_GROUPS[@]}"; do
		if [[ "$groups" = *"$group"* ]]; then
			printf "%b You are already in %s\\n" "${TICK}" "${group}"
		else
			printf "%b %bYou are not in group %s\\n" "${CROSS}" "${RED}" "${group}"
			printf "    Contact your supervisor to get access.%b\\n" "${RESET}"
		fi
	done
}

get_shell() {
	local parent_shell_pid
	parent_shell_pid=$(ps -o ppid= -p $$ | tr -d ' ')
	local shell_command
	shell_command=$(ps -p "$parent_shell_pid" -o comm=)
	echo "$shell_command"
}

restore_shell() {
	cp "$HOME"/dot.zshrc "$HOME"/.zshrc
	cp "$HOME"/dot.bashrc "$HOME"/.bashrc
	printf "%b Successfully restored everything. Exiting now...\\n" "${TICK}"
	exit 1
}

change_shell() {
	if [[ ! "$HOSTNAME" == *"login23"* ]]; then
		printf "%b %bYou are not on claix23. Aborting immediately...%b\\n" "${CROSS}" "${RED}" "${RESET}"
		exit 1
	fi
	if [[ -f "$HOME"/.bashrc ]]; then
		echo "Found an exisiting .bashrc."
		mv "$HOME"/.bashrc "$HOME"/old.bashrc && printf "%b Copied the existing .bashrc to ~/old.bashrc" "${TICK}"
	fi
	touch "$HOME"/.bashrc
	echo "source /usr/local_host/etc/bashrc" >>"$HOME"/.bashrc
	mv "$HOME"/.zshrc "$HOME"/old.zshrc && printf "%b Copied your current .zshrc to ~/old.zshrc\\n" "${TICK}"
	touch "$HOME"/.zshrc
	echo "source /usr/local_host/etc/switch_login_shell bash" >>"$HOME"/.zshrc
	printf "%bPlease open now a second connection to the cluster and check if you can login.\\n" "${RED}"
	printf "In your second connection run \"\$ ps -o comm= -p \$\$\". This should print out \"bash\"%b\\n" "${RESET}"
	while true; do
		read -r -p "Was this successful? [y/N] " -n 1
		case "$REPLY" in
		y | Y) {
			printf "%b %bSuccessfully changed your default shell.%b\\n" "${TICK}" "${GREEN}" "${RESET}"
			break
		} ;;
		n | N) {
			printf "%b %bCopying back the backups to restore status quo.%b\\n" "${CROSS}" "${GREEN}" "${RESET}"
			restore_shell
		} ;;
		*) {
			echo "Invalid input. Please try again or abort with Ctrl+C"
			continue
		} ;;
		esac
	done
}

show_logo

printf "This script will test some settings and setup your environment.\\n"
printf "(c) 2024 - Ole Osterthun\\n"
echo
printf "%bLet's check if you are already in all relevant user groups...%b\\n\\n" "${BLUE}" "${RESET}"
check_user
current_shell=$(get_shell)

echo
printf "%bLet's check which shell you are using...%b\\n\\n" "${BLUE}" "${RESET}"

if [[ "$current_shell" == *"bash"* ]]; then
	printf "%b You are already using bash\\n" "${TICK}"
else
	printf "%b %bYou are not using bash%b\\n" "${CROSS}" "${RED}" "${RESET}"
	echo
	while true; do
		read -r -p "Do you want to change your default shell to bash? (recommended) [y/N] " -n 1
		echo
		case "$REPLY" in
		y | Y) {
			change_shell
			break
		} ;;
		n | N) {
			echo "Nothing else to do then..."
			exit 0
		} ;;
		*) {
			echo "Invalid input. Try again or abort with Ctrl+C"
			continue
		} ;;
		esac
	done
fi

echo
printf "%bLet's install some dotfiles to make life a bit easier (strongly recommended)%b\\n\\n" "${BLUE}" "${RESET}"
while true; do
	read -r -p "Do you want to install the dotfiles? [y/N] " -n 1
	echo
	case "$REPLY" in
	y | Y) break ;;
	n | N) {
		echo "There is nothing else to do then."
		exit 0
	} ;;
	*) {
		echo "Invalid input. Please try again or abort with Ctrl+C"
		continue
	} ;;
	esac
done

printf "Cloning dotfiles to ~/dotfiles\\n"
git clone https://git.rwth-aachen.de/osterthun/rwth-dotfiles "$HOME"/dotfiles >/dev/null 2>&1
printf "%b Successfully cloned dotfiles\\n" "${TICK}"
mkdir -p "$HOME"/old_dotfiles
source "$HOME"/dotfiles/FILELIST
for f in "${DOTFILES[@]}"; do
	if [[ -f "$HOME"/"${f:3}" ]]; then
		mv "$HOME"/"${f:3}" "$HOME"/old_dotfiles/"${f:3}"
		printf "Backed up %s to ~/old_dotfiles\\n" "${f:3}"
	fi
	ln -fs "$HOME"/dotfiles/"$f" "$HOME"/"${f:3}"
	printf "%b Installed %s\\n" "${TICK}" "${f:3}"
done

echo
printf "%bLet's install a helper to view your personal queue (strongly recommended)%b\\n\\n" "${BLUE}" "${RESET}"
while true; do
	read -r -p "Do you want to install the queue helper? [y/N] " -n 1
	echo
	case "$REPLY" in
	y | Y) break ;;
	n | N) {
		echo "There is nothing else to do then."
		exit 0
	} ;;
	*) {
		echo "Invalid input. Please try again or abort with Ctrl+C"
		continue
	} ;;
	esac
done

if [[ ! -d "$HOME"/bin ]]; then
	mkdir -p "$HOME"/bin
fi
curl -s https://raw.githubusercontent.com/polyluxus/rwth-tools/master/queue-support/slurm/myq_slurm.sh > "$HOME"/bin/myq.sh
printf "%b Successfully installed helper\\n" "${TICK}"


echo
printf "%bYou are ready to go! Open a new session now.%b\\n" "${GREEN}" "${RESET}"
